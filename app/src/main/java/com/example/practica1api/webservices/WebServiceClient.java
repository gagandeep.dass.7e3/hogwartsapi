package com.example.practica1api.webservices;

import com.example.practica1api.model.Data;
import com.example.practica1api.model.Personaje;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface WebServiceClient {

    // NOS DEVUELVE TODOS LOS RESULTADOS DE LA API
    @GET("characters")
    Call<List<Personaje>> getPersonaje();


    // NOS FILTRA LOS RESULTADOS DE LA API
    @GET("characters/house/{house}") // characters/house/gryffindor ->  characters/house/{house}
    Call<List<Personaje>> getPersonaje (@Path("house") String house);

    /*
    @GET("house")
    //Call<Data> getPersonaje(@Query("house") String house);
    Call<List<Personaje>> getPersonaje (@Query("house") String house);
    //house?house=valorQueNosPasaUsuario

    @GET()
    Call<Data>getPersonajes(@Url String url);

    @GET()
    Call<Personaje>getPersonaje(@Url String url);

    @GET()
    Call<Data>getNextOrPreviousPage(@Url String url);
    */

}
