package com.example.practica1api.adapter;

import android.content.ClipData;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.practica1api.R;
import com.example.practica1api.activities.LoadCharacter;
import com.example.practica1api.model.Personaje;

import java.util.List;

public class MyAdapter  extends RecyclerView.Adapter<MyAdapter.MyAdapterHolder> {

    private List<Personaje> personajes;

    public MyAdapter(List<Personaje> personajes){ this.personajes = personajes; }

    public void setPersonajes(List<Personaje> personajes){
        this.personajes = personajes;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyAdapter.MyAdapterHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        /*
        int layout = 0;
        if (LoadCharacter.VISUALIZACION == LoadCharacter.LIST_VIEW){
            layout = R.layout.item_view;

        }else{
            layout = R.layout.activity_custom_grid_view;
        }

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(layout,viewGroup,false);
         */

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_view, viewGroup, false);
        //view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_custom_grid_view, viewGroup, false);

        return new MyAdapterHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyAdapterHolder holder, int position) {

        Personaje item = personajes.get(position);
        holder.textViewName.setText(item.getName());
        holder.textViewSpecie.setText(item.getSpecies());
        holder.textViewAnchestry.setText(item.getAncestry());
        holder.textViewPastronus.setText(item.getPatronus());

    }

    @Override
    public int getItemCount() {
        return personajes.size();
    }


    public static class MyAdapterHolder extends RecyclerView.ViewHolder{
        TextView textViewName;
        TextView textViewSpecie;
        TextView textViewAnchestry;
        TextView textViewPastronus;


        public MyAdapterHolder(@NonNull View itemView){
            super(itemView);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewSpecie = itemView.findViewById(R.id.textViewSpecie);
            textViewAnchestry = itemView.findViewById(R.id.textViewAnchestry);
            textViewPastronus = itemView.findViewById(R.id.textViewPastronus);
        }
    }

}
