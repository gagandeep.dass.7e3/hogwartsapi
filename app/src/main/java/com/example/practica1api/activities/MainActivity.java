package com.example.practica1api.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica1api.R;
import com.example.practica1api.adapter.MyAdapter;
import com.example.practica1api.model.Data;
import com.example.practica1api.model.Personaje;
import com.example.practica1api.webservices.WebServiceClient;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private TextView title;
    private Button gryffindor;
    private Button slytherin;
    private Button hufflepuff;
    private Button ravenclaw;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        title = findViewById(R.id.textViewTitle);
        gryffindor = findViewById(R.id.gryffindorBtn);
        slytherin = findViewById(R.id.slytherinBtn);
        hufflepuff = findViewById(R.id.hufflepuffBtn);
        ravenclaw = findViewById(R.id.ravenclawBtn);

        gryffindor.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Gryffindor", Toast.LENGTH_SHORT).show();
                travelActivities("Gryffindor");
            }
        });

        slytherin.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Slytherin", Toast.LENGTH_SHORT).show();
                travelActivities("Slytherin");
            }
        });

        hufflepuff.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //Toast.makeText(MainActivity.this, "Hufflepuff", Toast.LENGTH_SHORT).show();
                travelActivities("Hufflepuff");
            }
        });

        ravenclaw.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
               //Toast.makeText(MainActivity.this, "Ravenclaw", Toast.LENGTH_SHORT).show();
                travelActivities("Ravenclaw");
            }
        });

    }

    private void travelActivities(String houseName){
        /*CON ESTE INTENT PASAMOS EL NOMBRE DE LA CASA DE HOGWARTS A LA SEGUNDA
        ACTIVITY Y A SU VEZ VIAJAMOS A DICHA ACTIVITY*/
        Intent travel = new Intent(this, LoadCharacter.class);
        travel.putExtra("houseName", houseName);
        startActivity(travel);

    }


}