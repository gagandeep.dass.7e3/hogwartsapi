package com.example.practica1api.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practica1api.R;
import com.example.practica1api.adapter.MyAdapter;
import com.example.practica1api.model.Data;
import com.example.practica1api.model.Personaje;
import com.example.practica1api.webservices.WebServiceClient;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoadCharacter extends AppCompatActivity {

    /*
    public static final int LIST_VIEW = 1;
    public static final int GRID_VIEW = 2;
    public static int VISUALIZACION = LIST_VIEW;
    */
    //private RecyclerView.LayoutManager layoutManager;

    private Button home;
    private TextView houseNameText;

    private RecyclerView rView;
    private MyAdapter myAdapter;
    private List<Personaje> personajes;

    private Retrofit retrofit;
    private HttpLoggingInterceptor loggingInterceptor;
    private OkHttpClient.Builder httpClientBuilder;

    private String hogwartsHouseName;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_load_character);

        home = findViewById(R.id.backHomeButton);
        houseNameText = findViewById(R.id.textViewHouseName);

        rView = findViewById(R.id.recyclerView);
        rView.setLayoutManager(new LinearLayoutManager(this));
        personajes = new ArrayList<Personaje>();
        myAdapter = new MyAdapter(personajes);
        rView.setAdapter(myAdapter);

        hogwartsHouseName = getIntent().getExtras().getString("houseName");


        switch (hogwartsHouseName) {
            case "Gryffindor":
            case "gryffindor":
                houseNameText.setTextColor((getResources().getColor(R.color.gryffindor)));
                break;
            case "Slytherin":
            case "slytherin":
                houseNameText.setTextColor((getResources().getColor(R.color.slytherin)));
                break;
            case "Hufflepuff":
            case "hufflepuff":
                houseNameText.setTextColor((getResources().getColor(R.color.hufflepuff)));
                break;
            case "Ravenclaw":
            case "ravenclaw":
                houseNameText.setTextColor((getResources().getColor(R.color.ravenclaw)));
                break;
            default:
                houseNameText.setTextColor((getResources().getColor(R.color.white)));
                break;
        }
        houseNameText.setText("Casa de " + hogwartsHouseName); //TITULO AL CARGAR SEGÚN LA CASA SELECCIONADA


        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(LoadCharacter.this, "Home", Toast.LENGTH_SHORT).show();
                travelActivities();
            }
        });

        lanzarPeticion();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        return true;
    }

    private void travelActivities() {
        Intent travel = new Intent(this, MainActivity.class);
        startActivity(travel);
    }

    private void lanzarPeticion() {
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl("http://hp-api.herokuapp.com/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();

        WebServiceClient client = retrofit.create(WebServiceClient.class);

        Call<List<Personaje>> call = client.getPersonaje(hogwartsHouseName);

        call.enqueue(new Callback<List<Personaje>>() {
            @Override
            public void onResponse(Call<List<Personaje>> call, Response<List<Personaje>> response) {
                List<Personaje> personajes = response.body();
                assert personajes != null;
                for (Personaje personaje : personajes) {
                    String content = "";
                    // content = , content += -> same
                    content += "Name: " + personaje.getName();
                    content += "Species: " + personaje.getSpecies();
                    content += "Anchestry: " + personaje.getAncestry();
                    content += "Patronus: " + personaje.getPatronus();

                    myAdapter.setPersonajes(personajes);
                    //Toast.makeText(LoadCharacter.this,content,Toast.LENGTH_SHORT).show();
                }
                if (personajes == null) {
                    Toast.makeText(LoadCharacter.this, "Hubo un error al carga la api", Toast.LENGTH_SHORT).show();
                } else {
                    String mensaje = "Casa de Hogwarts: ";
                    //Toast.makeText(LoadCharacter.this,"La api se ha cargado correctamente",Toast.LENGTH_SHORT).show();
                    switch (hogwartsHouseName) {
                        case "Gryffindor":
                        case "gryffindor":
                            Toast.makeText(LoadCharacter.this, mensaje + "Gryffindor", Toast.LENGTH_SHORT).show();
                            break;
                        case "Slytherin":
                        case "slytherin":
                            Toast.makeText(LoadCharacter.this, mensaje + "Slytherin", Toast.LENGTH_SHORT).show();
                            break;
                        case "Hufflepuff":
                        case "hufflepuff":
                            Toast.makeText(LoadCharacter.this, mensaje + "Hufflepuff", Toast.LENGTH_SHORT).show();
                            break;
                        case "Ravenclaw":
                        case "ravenclaw":
                            Toast.makeText(LoadCharacter.this, mensaje + "Ravenclaw", Toast.LENGTH_SHORT).show();
                            break;
                        default:
                            Toast.makeText(LoadCharacter.this, "Error al cargar la casa", Toast.LENGTH_SHORT).show();
                            break;
                    }

                }

                //myAdapter.setPersonajes(personajes);
            }

            @Override
            public void onFailure(Call<List<Personaje>> call, Throwable t) {
                Log.d("TAG1", "Error" + t.getMessage());
            }
        });
    }


    public boolean onOptionsItemSelected(@NonNull MenuItem item){
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.changeViewListButton:
                //VISUALIZACION = LIST_VIEW;
                //rView.setLayoutManager(new LinearLayoutManager(this));
                //Toast.makeText(LoadCharacter.this, "List View", Toast.LENGTH_SHORT).show();
                return true;
            case R.id.changeViewGridButton:
                //Toast.makeText(LoadCharacter.this, "Grid View", Toast.LENGTH_SHORT).show();
                //VISUALIZACION = GRID_VIEW;
                //rView.setLayoutManager(new GridLayoutManager(this,2));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}