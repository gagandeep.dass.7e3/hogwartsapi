package com.example.practica1api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Personaje {
    private String name;
    private String species;
    private String ancestry;
    private String patronus;

    public List<Personaje> getResults() {
        return results;
    }

    public void setResults(List<Personaje> results) {
        this.results = results;
    }

    private List<Personaje> results;

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getAncestry() {
        return ancestry;
    }

    public void setAncestry(String ancestry) {
        this.ancestry = ancestry;
    }

    public String getPatronus() {
        return patronus;
    }

    public void setPatronus(String patronus) {
        this.patronus = patronus;
    }




}
